Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Mail-Mbox-MessageParser
Upstream-Contact: David Coppit <david@coppit.org>
Source: https://metacpan.org/release/Mail-Mbox-MessageParser

Files: *
Copyright: 1998-2000, Broc Seib <bseib@purdue.edu>
 2000-2018, David Coppit <david@coppit.org>
License: GPL-2

Files: inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Sort/Versions.pm
Copyright: 1996, Kenneth J. Albanowski <kjahds@kjahds.com>
License: Artistic or GPL-1+

Files: inc/File/Slurper.pm
Copyright: 2014, Leon Timmermans <leont@cpan.org>
License: Artistic or GPL-1+

Files: inc/URI/Escape.pm
Copyright: 1995-2009, Gisle Aas <gisle@activestate.com>
 1998, Graham Barr <gbarr@pobox.com>
 1995, Martijn Koster <mak@surfski.webcrawler.com>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2003-2007, Joey Hess <joeyh@debian.org>
 2008-2023, gregor herrmann <gregoa@debian.org>
 2009, Jonathan Yu <jawnsy@cpan.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
